﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Character : UnitData {
	public string Class;
	public int experience;
	public List<Grimoire> grimoires;

	public Character () {
		Class = "summoner";
		level = 1;
		experience = 0;
		maxHP = 40;
		HP = maxHP;
		status = "None";
		force = 10;
		will = 12;
		speed = 5;
		agility = 3;
		luck = 1;
		forceAlignment = new List<int>();
		willAlignment = new List<int>();
		grimoires = new List<Grimoire>();
		weapon = new Weapon();
		armour = new Armour();
		accessory = new Accessory();
	}
}