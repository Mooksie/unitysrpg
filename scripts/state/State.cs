﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class State {
	public static Dictionary<string, Character> characters = new Dictionary<string, Character>();
	public static List<Item> inventory = new List<Item>();
	public static Castle castle = new Castle();
	public static List<int> flags = new List<int>();
	public static Settings settings = new Settings();
	public static Encyclopedia encyclopedia = new Encyclopedia();
}