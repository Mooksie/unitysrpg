using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Settings {
	public int newTurnCentreMode = 1; //Off, Always, PC
	public int moveCentreMode = 0; //
	public float battleMoveSpeed = 2f;
	public Dictionary<string, KeyCode> keyBindings = new Dictionary<string, KeyCode> {
		{"cameraRotateUp", KeyCode.UpArrow},
		{"cameraRotateDown", KeyCode.DownArrow},
		{"cameraRotateLeft", KeyCode.LeftArrow},
		{"cameraRotateRight", KeyCode.RightArrow},
		{"cameraMoveUp", KeyCode.W},
		{"cameraMoveDown", KeyCode.S},
		{"cameraMoveLeft", KeyCode.A},
		{"cameraMoveRight", KeyCode.D},
		{"cameraZoomIn", KeyCode.Z},
		{"cameraZoomOut", KeyCode.X},
		{"battleEndTurn", KeyCode.Space}};
}
