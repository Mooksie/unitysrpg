using UnityEngine;
using System.Collections;

public static class Vector3Extensions {
	public static Vector2 xz (this Vector3 v) {
		return new Vector2(v.x, v.z);
	}
}

public class Location {
	public int a, b, h;
	public Location(int _a, int _b, int _h) {
		a = _a;
		b = _b;
		h = _h;
	}
	public static Location operator +(Location loc1, Location loc2) {
		return new Location(loc1.a+loc2.a, loc1.b+loc2.b, loc1.h+loc2.h);
	}
	public static float Distance(Location loc1, Location loc2) {
		return Mathf.Sqrt((loc1.a*loc2.a)*(loc1.a*loc2.a) + (loc1.b*loc2.b)*(loc1.b*loc2.b) + (loc1.h*loc2.h)*(loc1.h*loc2.h));
	}
	public Location2D ab () {
		return new Location2D(a, b);
	}
	public Vector3 toPosition() {
		return new Vector3(a - 0.5f * b, TileManager.tileHeight * h, 0.8655f * b);
	}
}

public class Location2D {
	public int a, b;
	public Location2D(int _a, int _b) {
		a = _a;
		b = _b;
	}
	public static Location2D operator +(Location2D loc1, Location2D loc2) {
		return new Location2D(loc1.a+loc2.a, loc1.b+loc2.b);
	}
	public static float Distance(Location2D loc1, Location2D loc2) {
		return Mathf.Sqrt((loc1.a*loc2.a)*(loc1.a*loc2.a) + (loc1.b*loc2.b)*(loc1.b*loc2.b));
	}
	public override int GetHashCode()
	{
		return a ^ b;
	}
	public override bool Equals(object obj)
	{
		if (a == ((Location2D)obj).a && b == ((Location2D)obj).b) {
			return true;
		}
		return false;
	}
}