﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Saver : MonoBehaviour {
	public static BinaryFormatter b = new BinaryFormatter();
	public static FileStream f;

	public static void Save()
	{
		f = File.Create("./save.dat");
		b.Serialize(f, new SaveState());	
		f.Close();
	}
	
	public static void Load()
	{
		if(File.Exists("./save.dat"))
		{
			f = File.Open("./save.dat", FileMode.Open);
			((SaveState)b.Deserialize(f)).Load();
			f.Close();	
		}
	}
}

[System.Serializable]
public class SaveState {
	public Dictionary<string, Character> characters;
	public List<Item> inventory;
	public Castle castle;
	public List<int> flags;
	public Settings settings;
	public Encyclopedia encyclopedia;

	public SaveState() {
		characters = State.characters;
		inventory = State.inventory;
		castle = State.castle;
		flags = State.flags;
		settings = State.settings;
		encyclopedia = State.encyclopedia;
	}
	public void Load() {
		State.characters = characters;
		State.inventory = inventory;
		State.castle = castle;
		State.flags = flags;
		State.settings = settings;
		State.encyclopedia = encyclopedia;
	}
}