using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BattleObject : Raisables {
	public int a, b, h;
	public int oldA, oldB, oldH;
	public string objName;
	public int dir;
	public bool passable;
	public bool attackable = true;
	public int dirAngle;
	public float height;
	
	public void define (BattleObjectStruct mapObjectStruct) {
		a = mapObjectStruct.a;
		b = mapObjectStruct.b;
		h = mapObjectStruct.h;
		oldA = a;
		oldB = b;
		oldH = h;
		height = mapObjectStruct.height;
		dir = (int)transform.rotation.y;
		transform.parent = ObjectManager.objects.transform;
		transform.name = mapObjectStruct.objName;
	}
	public override void raise (Raisables source, Alert alert)
	{
		throw new System.NotImplementedException ();
	}
	void OnMouseUpAsButton() {
		Battle.raise (this, new ClickObjAlert(this));
	}
	public void move() {
		
	}
}

