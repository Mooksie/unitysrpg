using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EU : Unit {
	public void processTurn() {

	}
}

public class Enemy : UnitData {
	public int experienceGiven;

	public void init () {
		level = 1;
		experienceGiven = 20;
		maxHP = 40;
		HP = maxHP;
		status = "None";
		force = 10;
		will = 12;
		speed = 5;
		agility = 3;
		luck = 1;
		forceAlignment = new List<int>();
		willAlignment = new List<int>();
		weapon = new Weapon();
		armour = new Armour();
		accessory = new Accessory();
	}
}