using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Unit : BattleObject {
	public Animator animator;
	public Dictionary<Location2D, List<Location2D>> routes;
	public List<Location2D> attackTargets;
	public BattleObject attackTarget;
	public List<Location2D> currentRoute;

	public Vector3 moveStartDrawLoc;
	public Vector3 targetDrawLoc;
	public Vector3 direction;
	
	public string action = null;
	public bool moved = false;
	public bool attacked = false;

	public UnitData data;
	
	public void init (BattleObjectStruct dd, UnitData cc) { //Needs a, b, h, objName | class, level, xp, hp, status, force, will, speed, agility, luck, forceAlignment, willAlignment, grimoires, weapon, armour, accessory
		base.define (dd);
		animator = this.GetComponent<Animator>();
		routes = new Dictionary<Location2D, List<Location2D>>();
		attackTargets = new List<Location2D>();
		attackTarget = null;
		currentRoute = new List<Location2D>();
		data = cc;
	}
	#region Route generation
	public void genRoutes() {
		routes = new Dictionary<Location2D, List<Location2D>>();
		attackTargets = new List<Location2D>();
		List<Location2D> current = new List<Location2D>() {new Location2D((int)a, (int)b)};
		List<Location2D> nextItr = new List<Location2D>();
		List<Location2D> sweep;
		if (!(moved && attacked)) {
			for (int i = 1; i <= data.agility; i++) {
				foreach (Location2D tile in current) {
					sweep = new List<Location2D>() { tile + new Location2D(-1, -1), tile + new Location2D(0, -1), 
						tile + new Location2D(-1, 0), tile + new Location2D(1, 0), tile + new Location2D(0, 1), tile + new Location2D(1, 1)};
					if (i % 2 == 1) {sweep.Reverse();}
					foreach (Location2D aTile in sweep) {
						if (TileManager.mData.ContainsKey (aTile.a, aTile.b)) { //Tile present
							if (!(routes.ContainsKey(new Location2D(aTile.a, aTile.b)))) { //Tile not checked
								if (TileManager.mData[aTile.a, aTile.b].passable) { //Tile passable
									if (TileManager.mData[aTile.a, aTile.b].h <= TileManager.mData[tile.a, tile.b].h + (int)(data.agility/2)) { //Tile not too high
										bool ally, moveable, attackable = false;
										bool objPresent = ObjectManager.oData.ContainsKey(aTile.a, aTile.b);
										BattleObject obj = null;
										if (objPresent) {
											obj = ObjectManager.oData[aTile.a, aTile.b];
											ally = obj.GetType() == this.GetType();
											moveable = ally || (obj is NU && obj.passable);
											attackable = !attacked && !ally && obj.attackable;
										} else {
											moveable = true;
										}
										if (attackable) {
											attackTargets.Add (aTile);
										} else if (moveable) {
											nextItr.Add(aTile);
											if (routes.ContainsKey (tile)) {
												routes.Add (aTile, new List<Location2D>(routes[tile]));
											} else {
												routes.Add (aTile, new List<Location2D>());
											}
											routes[aTile].Add(aTile);
										}
									}
								}
							}
						}
					}
				}
				current = new List<Location2D>(nextItr);
				nextItr = new List<Location2D>();
			}
			foreach (var obj in ObjectManager.oData.dict.Values) {
				if (obj is Unit) {
					if (routes.ContainsKey (new Location2D(obj.a, obj.b))) {
						routes.Remove (new Location2D(obj.a, obj.b));
					}
				}
			}
		}
	}
	#endregion
	#region Actions
	public Vector3 getDirection(Location2D target) {
		Vector2 tempDir = new Vector2((target.a - a), (target.b - b));
		dirAngle = (int)(120 - tempDir.x * 90 + ((tempDir.y == 0) ? 60 : 0) + ((tempDir.x == 0) ? 30 : 0));
		if (tempDir.x == 0 && tempDir.y == 1)
		{dirAngle += 180;}
		transform.rotation = Quaternion.Euler (0f, dirAngle, 0f);

		if (tempDir.y == 0) {
			tempDir = new Vector2(tempDir.x, 0f).normalized;
		} else if (tempDir.x == 0) {
			tempDir = new Vector2(-0.5f * tempDir.y, 0.8660254f * tempDir.y).normalized;
		} else {
			tempDir = new Vector2(0.5f * tempDir.x, 0.8660254f * tempDir.y).normalized;
		}
			/*if (tempDir == new Vector2(0f, -1f)) {
				tempDir = new Vector2(0.5f, 0.8660254f * tempDir[1]).normalized;
			} else if (tempDir == new Vector2(0, 1)) {
				tempDir = new Vector2(-0.5f, 0.8660254f * tempDir[1]).normalized;
			} else {
				tempDir = new Vector2(tempDir.x * 0.5f, 0.8660254f * tempDir[1]).normalized;
			}
		}*/
		return new Vector3(tempDir.x, 0f, tempDir.y);
	}
	public void startMove(Tile target) {
		moved = true;
		currentRoute = new List<Location2D>(routes[new Location2D(target.a, target.b)]);
		direction = getDirection (currentRoute[0]);
		oldA = a;
		oldB = b;
		oldH = h;
		moveStartDrawLoc = transform.position;
		targetDrawLoc = new Location (currentRoute[0].a, currentRoute[0].b, TileManager.mData[currentRoute[0]].h).toPosition();
		animator.SetBool("walking", true);
	}
	public bool updateMove() {
		if (currentRoute.Count != 0) {
			AnimatorStateInfo aa = animator.GetCurrentAnimatorStateInfo(0);
			if (true) {
				Vector3 translation = direction * State.settings.battleMoveSpeed * Time.deltaTime;
				if (targetDrawLoc.y != transform.position.y) { // Jumping
					if (!animator.GetBool ("jumping")) {
						animator.SetBool("jumping", true);
						animator.SetBool("walking", false);
					}
					if (aa.IsName("In Air")) {
						float jumpHeight = Mathf.Clamp (((Mathf.Abs(targetDrawLoc.y - moveStartDrawLoc.y) - 1) * 2f + 2), 2, 5);
						float b = (targetDrawLoc.y - moveStartDrawLoc.y) / jumpHeight;
						float s = jumpHeight * (Vector2.Distance (transform.position.xz(), targetDrawLoc.xz()) + b) * Vector2.Distance (transform.position.xz(), moveStartDrawLoc.xz());
						transform.Translate (translation, relativeTo:Space.World);
						transform.position = new Vector3(transform.position.x, moveStartDrawLoc.y + s, transform.position.z);
					}
				} else if (!(aa.IsName ("land"))) {	 // Walking
					transform.Translate (translation, relativeTo:Space.World);
				}
				if (Vector2.Distance(transform.position, targetDrawLoc) < 0.05) {
					a = currentRoute[0].a;
					b = currentRoute[0].b;
					h = TileManager.mData[currentRoute[0]].h;
					transform.position = new Location (a, b, h).toPosition();
					currentRoute.RemoveAt (0);
					if (currentRoute.Count != 0) {
						direction = getDirection (currentRoute[0]);
						targetDrawLoc = new Location (currentRoute[0].a, currentRoute[0].b, TileManager.mData[currentRoute[0]].h).toPosition();
						moveStartDrawLoc = transform.position;
					}
					animator.SetBool("jumping", false);
					animator.SetBool("walking", true);
				}
			}
		} else {
			direction = new Vector2();
			currentRoute = new List<Location2D>();
			animator.SetBool("walking", false);
			animator.SetBool("jumping", false);
			return true;
		}
		return false;
	}
	public void startAttack() {
		animator.SetBool("attacking", true);
		attacked = true;
	}
	public bool updateAttack() {
		animator.SetBool("attacking", false);
		return true;
	}
	public void undoMove() {
		a = oldA;
		b = oldB;
		h = oldH;
		transform.position = new Location(a, b, h).toPosition();
		moved = false;
		direction = new Vector2();
		currentRoute = new List<Location2D>();
		animator.SetBool("walking", false);
		animator.SetBool("jumping", false);
		animator.Play ("idle");
	}
	#endregion
}