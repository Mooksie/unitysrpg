﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnitData {
	public int level;
	public int maxHP;
	public int HP;
	public string status;
	public int force;
	public int will;
	public int speed;
	public int agility;
	public int luck;
	public List<int> forceAlignment;
	public List<int> willAlignment;
	public Weapon weapon;
	public Armour armour;
	public Accessory accessory;
}