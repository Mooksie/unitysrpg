﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseTile : Raisables {
	public int a, b, h;
	public string shape, terrain;
	public virtual void define (TileStruct tileStruct) {
		a = tileStruct.a;
		b = tileStruct.b;
		h = tileStruct.h;
		shape = tileStruct.shape;
		terrain = tileStruct.terrain;
		
		renderer.material = TileManager.mapMaterials[terrain];
		name = "tile a:" + a + " b:" + b + " h:" + h + " t:" + terrain;
		transform.parent = TileManager.map.transform;
	}
	public override void raise (Raisables source, Alert alert)
	{
		throw new System.NotImplementedException ();
	}
}

public class Tile : BaseTile {
	public List<BaseTile> baseTiles;
	public bool passable;
	
	public override void define (TileStruct tileStruct) {
		base.define (tileStruct);
		passable = tileStruct.passable;
		baseTiles = new List<BaseTile>();
	}
	void OnMouseUpAsButton() {
		Battle.raise (this, new ClickTileAlert(this));
	}
}