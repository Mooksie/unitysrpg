using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectContainer {
	public Dictionary<Location2D, BattleObject> dict = new Dictionary<Location2D, BattleObject>();
	
	public void Add(BattleObject mobj) {
		if (!dict.ContainsKey(new Location2D(mobj.a, mobj.b))) {
			this[mobj.a, mobj.b] = mobj;
		}
	}
	public void Remove(int a, int b){
		dict.Remove (new Location2D(a, b));
	}
	public BattleObject this[int a, int b]{
		get {
			if (dict.ContainsKey (new Location2D(a, b))) {
				return dict[new Location2D(a, b)];
			} else {
				return null;
			}
		}
		set {dict.Add (new Location2D(a, b), value);}
	}
	public BattleObject this[Location2D loc]{
		get {
			if (dict.ContainsKey (loc)) {
				return dict[loc];
			} else {
				return null;
			}
		}
		set {dict.Add (loc, value);}
	}
	public bool ContainsKey (int a, int b) {
		return dict.ContainsKey(new Location2D(a, b));
	}
	public bool ContainsKey (Location2D loc) {
		return dict.ContainsKey(loc);
	}
	public void move(Location2D oldKey, Location2D newKey) {
		if (this.ContainsKey(newKey.a, newKey.b)) {
			this[newKey.a, newKey.b] = this[oldKey.a, oldKey.b];
			dict.Remove (oldKey);
		}
	}
}

public class ObjectManager : Raisables {
	public static GameObject objects;
	public static ObjectContainer oData;
	
	void Start () {
		generateObjects(LevelData.test.objects);
	}
	public override void raise(Raisables source, Alert alert) {
		if (alert is MoveObjAlert) {
			oData.move(((MoveObjAlert)alert).oldLoc, ((MoveObjAlert)alert).newLoc);
		}
	}
	#region Creating objects
	void generateObjects(List<BattleObjectStruct> objectData) {
		objects = new GameObject("Objects");
		oData = new ObjectContainer();
		foreach (var objStruct in objectData) {
			GameObject mobj = Instantiate(Resources.Load("prefabs/"+objStruct.type), new Location(objStruct.a, objStruct.b, objStruct.h).toPosition(), Quaternion.identity) as GameObject;
			if (objStruct.type == "PU") {
				((Unit)mobj.GetComponent<Unit>()).init (objStruct, State.characters[objStruct.objName]);

			}
			else if (objStruct.type == "EU") {
				((Unit)mobj.GetComponent<Unit>()).init (objStruct, new Enemy());
			}
			else if (objStruct.type == "NU") {
				((Unit)mobj.GetComponent<Unit>()).init (objStruct, new NeutralUnit());
			}
			oData.Add ((Unit)mobj.GetComponent<Unit>());
		}
	}
	#endregion
}