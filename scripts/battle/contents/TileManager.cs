using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TileContainer {
	public Dictionary<Location2D, Tile> dict = new Dictionary<Location2D, Tile>();
	
	public void Add(Tile tile) {
		if (!dict.ContainsKey(new Location2D(tile.a, tile.b))) {
			this[tile.a, tile.b] = tile;
		}
	}
	public Tile this[int a, int b]{
		get {
			if (dict.ContainsKey (new Location2D(a, b))) {
				return dict[new Location2D(a, b)];
			} else {
				return null;
			}
		}
		set {dict.Add (new Location2D(a, b), value);}
	}
	public Tile this[Location2D loc]{
		get {
			if (dict.ContainsKey (loc)) {
				return dict[loc];
			} else {
				return null;
			}
		}
		set {dict.Add (loc, value);}
	}
	public bool ContainsKey (int a, int b) {
		return dict.ContainsKey(new Location2D(a, b));
	}
	public bool ContainsKey (Location2D loc) {
		return dict.ContainsKey(loc);
	}
}

public class TileManager : Raisables {
	public static GameObject map;
	public static TileContainer mData;
	
	public static int mapSize = LevelData.mapSize;
	public static float tileHeight;
	public static float tileSize;
	
	public static Dictionary<string, Material> mapMaterials = new Dictionary<string, Material>() {
		{"G", null}, {"D", null}, {"W", null}, {"A", null}, {"MT", null}, {"AT", null}};
	
	public Unit highlightedRoutes = null;
	
	void Awake () {
		tileHeight = ((GameObject)Resources.Load ("prefabs/tile")).transform.localScale.z * 2f;
		tileSize = 1f;//((GameObject)Resources.Load ("prefabs/tile")).transform.localScale.x;
		mapMaterials["G"] = Resources.Load("materials/GMaterial") as Material;
		mapMaterials["D"] = Resources.Load("materials/DMaterial") as Material;
		mapMaterials["W"] = Resources.Load("materials/WMaterial") as Material;
		mapMaterials["A"] = Resources.Load("materials/activeMaterial") as Material;
		mapMaterials["MT"] = Resources.Load("materials/moveTargetMaterial") as Material;
		mapMaterials["AT"] = Resources.Load("materials/attackTargetMaterial") as Material;
	}
	void Start () {
		Battle.subscribe("newTurn", this);
		Battle.subscribe("startObjMove", this);
		Battle.subscribe("moveObj", this);
		generateMap(LevelData.test.tiles);
	}
	public override void raise(Raisables source, Alert alert) {
		if (alert is NewTurnAlert) {
			deHighlightRoutes ();
			highlightedRoutes = ((NewTurnAlert)alert).unit;
			highlightRoutes ();
		}
		else if (alert is StartObjMoveAlert) {
			deHighlightRoutes ();
		}
		else if (alert is MoveObjAlert) {
			highlightRoutes ();
		}
	}
	#region Creating map
	void generateMap (List<TileStruct> tileData) {
		map = new GameObject("MapTiles");
		mData = new TileContainer();
		foreach (var tileStruct in LevelData.test.tiles) {
			Tile tile = (Instantiate(Resources.Load("prefabs/tile"), new Location(tileStruct.a, tileStruct.b, tileStruct.h).toPosition(), Quaternion.Euler(270f, 0f, 0f)) as GameObject).GetComponent<Tile>();
			tile.define (tileStruct);
			mData.Add (tile);
		}
		foreach (var tile in mData.dict.Values) { // Basetiles
			int lowestAdjacent = lowestAdjacentTile(tile);
			if (lowestAdjacent != -1) {
				for (int i = tile.h - 1; i > (tile.h - Mathf.Abs(lowestAdjacent - tile.h) - 1); i--) {
					BaseTile baseTile = (Instantiate(Resources.Load("prefabs/tile"), new Location(tile.a, tile.b, i).toPosition(), Quaternion.Euler(270f, 0f, 0f)) as GameObject).GetComponent<BaseTile>();
					baseTile.define (new TileStruct(tile.a, tile.b, i, "tile", "D", true));
					mData[tile.a, tile.b].baseTiles.Add(baseTile);
				}
			}
		}
    } 
	public int lowestAdjacentTile(Tile tile) {
		int aCheck, bCheck;
		int lowest = tile.h;
		int[,] dirs = new int[6, 2]{{-1, -1}, {0, -1}, {-1, 0}, {1, 0}, {0, 1}, {1, 1}};
		for (int i = 0; i < 6; i ++) {
			aCheck = tile.a+dirs[i, 0];
			bCheck = tile.b+dirs[i, 1];
			if (mData.ContainsKey(aCheck, bCheck)) {
				lowest = Mathf.Min(lowest, mData[aCheck, bCheck].h);
			} else {
				lowest = 0;
			}
		}
		if (lowest == tile.h) {
			return -1;
		} else {
			return lowest;
		}
	}
	#endregion
	#region Changing map appearance
	public void highlightTile(int x, int z, string mapMaterial) {
		mData[x, z].transform.renderer.material = mapMaterials[mapMaterial];
	}
	public void deHighlightTile(int x, int z) {
		mData[x, z].transform.renderer.material = mapMaterials[mData[x, z].terrain];
	}
	public void highlightRoutes() {
		highlightTile (highlightedRoutes.a, highlightedRoutes.b, "A");
		if (!(highlightedRoutes.moved)) {
			foreach (Location2D tile in highlightedRoutes.routes.Keys) {
				highlightTile (tile.a, tile.b, "MT");
			}
		}
		if (!(highlightedRoutes.attacked)) {
			foreach (Location2D tile in highlightedRoutes.attackTargets) {
				highlightTile (tile.a, tile.b, "AT");
			}
		}
	}
	public void deHighlightRoutes() {
		if (highlightedRoutes != null) {
			deHighlightTile (highlightedRoutes.a, highlightedRoutes.b);
			foreach (Location2D tile in highlightedRoutes.routes.Keys) {
				deHighlightTile (tile.a, tile.b);
			}
			foreach (Location2D tile in highlightedRoutes.attackTargets) {
				deHighlightTile (tile.a, tile.b);
			}
		}
	}
	#endregion
}