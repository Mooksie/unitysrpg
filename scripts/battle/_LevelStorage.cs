using UnityEngine;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public struct LevelStruct {
	public List<TileStruct> tiles;
	public List<BattleObjectStruct> objects;
	public LevelStruct(List<TileStruct> tilesn, List<BattleObjectStruct> objectsn) {
		tiles = tilesn;
		objects = objectsn;
	}
}

public struct TileStruct {
	public int a, b, h;
	public string shape, terrain;
	public bool passable;
	public TileStruct(int an, int bn, int hn, string shapen, string terrainn, bool passablen) {
		a = an;
		b = bn;
		h = hn;
		shape = shapen;
		terrain = terrainn;
		passable = passablen;
	}
}

public struct BattleObjectStruct {
	public string type;
	public int a, b, h;
	public string model, objName;
	public float height;
	public BattleObjectStruct(string typen, string namen, int an, int bn, int hn, string modeln, float heightn) {
		type = typen;
		objName = namen;
		a = an;
		b = bn;
		h = hn;
		model = modeln;
		height = heightn;
	}
}
