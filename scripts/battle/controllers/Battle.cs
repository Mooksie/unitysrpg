//using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Battle : MonoBehaviour {
	static Dictionary<string, List<Raisables>> subscribers;
	#region Event manager
	void Awake() {
		subscribers = new Dictionary<string, List<Raisables>>();
	}
	
	public static void subscribe(string alertType, Raisables subscriber) {
		if (!subscribers.ContainsKey(alertType)) {
			subscribers.Add(alertType, new List<Raisables>());
		}
		subscribers[alertType].Add(subscriber);
	}
	
	public static void raise(Raisables source, Alert alert) {
		foreach (var subscriber in subscribers[alert.type]) {
			subscriber.raise(source, alert);
		}
	}
	#endregion
	#region Static helper functions
	/*public static bool getPointInHex(Vector2 point, float radius) {
		if ( ((point.y) > radius) ||
		((point.x * Mathf.Cos(30) + point.y * Mathf.Sin(30)) > radius) ||
		((point.x * Mathf.Cos(-30) + point.y * Mathf.Sin(-30)) > radius) ) {
			return false;
		} else {
			return true;
		}
	}*/
	#endregion
}

public abstract class Raisables : MonoBehaviour{
	public abstract void raise(Raisables source, Alert alert);
}

// Alert types: newTurn, startObjMove, clickTile, clickObj, endTurn, moveObj
public class Alert {public string type;}
public class StartObjMoveAlert : Alert {
	public Unit unit;
	public StartObjMoveAlert(Unit nUnit) {type = "startObjMove"; unit = nUnit;}
}
public class NewTurnAlert : Alert {
	public Unit unit;
	public NewTurnAlert(Unit nUnit) {type = "newTurn"; unit = nUnit;}
}
public class ClickTileAlert : Alert {
	public Tile tile;
	public ClickTileAlert(Tile nTile) {type = "clickTile"; tile = nTile;}
}
public class ClickObjAlert : Alert {
	public BattleObject mobj;
	public ClickObjAlert(BattleObject nMobj) {type = "clickObj"; mobj = nMobj;}
}
public class EndTurnAlert : Alert {
	public EndTurnAlert() {type = "endTurn";}
}
public class MoveObjAlert : Alert {
	public Location2D oldLoc, newLoc;
	public MoveObjAlert(Location2D nOldLoc, Location2D nNewLoc) {type = "moveObj"; oldLoc = nOldLoc; newLoc = nNewLoc;}
}