using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseState {}
public class NoState : BaseState {}
public class PausedState : BaseState {}
public class WaitState : BaseState {}
public class MovingState : BaseState {}
public class AttackingState : BaseState {}

public class Turns {
	public Unit unit;
	public int turnCounter;
	public int baseTurns;
	public Turns(Unit c, int t) {
		unit = c;
		turnCounter = t;
		baseTurns = t;
	}
	public void resetTurns() {
		turnCounter = baseTurns;
	}
}

public class BattleController : Raisables {
	public const int TURN_QUEUE_LENGTH = 6;
	public const int SPD_MAX = 15;
	public BattleObject activeUnit = null;
	public BattleObject selectedObj = null;
	public BaseState state = new NoState();
	public static List<Unit> turnQueue = new List<Unit>();
	public List<Turns> speedNumbers = new List<Turns>();
	
	void Start () {
		Battle.subscribe("endTurn", this);
		Battle.subscribe("clickObj", this);
		Battle.subscribe("clickTile", this);
		foreach (var o in ObjectManager.oData.dict.Values) {
			if (o is Unit) {
				speedNumbers.Add (new Turns((Unit)o, SPD_MAX - ((Unit)o).data.speed));
			}
		}
		generateTurns ();
		changeActive((Unit)turnQueue[0]);
	}
	void Update() {
		#region Temporary
		if (Input.GetKeyDown (State.settings.keyBindings["battleEndTurn"])) { // TEMPORARY - Put into HUD with button click
			endTurn();
		} else if (Input.GetKeyDown (KeyCode.S)) {
			Saver.Save();
		} else if (Input.GetKeyDown (KeyCode.L)) {
			Saver.Load();
		} else
		#endregion
		if (activeUnit is PU) {
			if (state is MovingState) {
				updateMove ();
			} else if (state is AttackingState) {
				updateAttack ();
			}
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (state is NoState || state is MovingState) {
					undoMove ();
				}
			}
		}
		else if (activeUnit is EU) {
			//((EU)activeUnit).processTurn();
		}
	}
	public override void raise(Raisables source, Alert alert) {
		if (alert is EndTurnAlert) {
			endTurn ();
		}
		else if (alert is ClickTileAlert) {
			clickTile (((ClickTileAlert)alert).tile);
		}
		else if (alert is ClickObjAlert) {
			clickObj (((ClickObjAlert)alert).mobj);
		}
	}
	#region Turn control
	public void generateTurns() {
		while (turnQueue.Count <= TURN_QUEUE_LENGTH) {
			for (int i = 0; i < speedNumbers.Count; i++) {
				speedNumbers[i].turnCounter--;
				if (speedNumbers[i].turnCounter <= 0 && (turnQueue.Count == 0 || (speedNumbers[i].unit != turnQueue[turnQueue.Count-1]))) {
					speedNumbers[i].resetTurns();
					turnQueue.Add (speedNumbers[i].unit);
				}
			}
		}
	}
	public void endTurn() {
		if (activeUnit != null) {
			((Unit)activeUnit).attacked = false;
			((Unit)activeUnit).moved = false;
		}
		turnQueue.RemoveAt (0);
		changeActive((Unit)turnQueue[0]);
		generateTurns ();
	}
	#endregion
	#region Selecting objects
	public void clickTile(Tile tile) {
		if (false) {// action for tile selected
			///
		} else if (state is NoState && activeUnit is PU && !((PU)activeUnit).moved) {
			startMove(tile);
		}
	}
	public void clickObj(BattleObject mobj) {
		selectedObj = mobj;
		if (false) {// action for object selected
			///
		}
	}
	public void changeActive(Unit newActive) {
		activeUnit = newActive;
		selectedObj = null;
		if (activeUnit is PU) {
			((PU)activeUnit).genRoutes ();
			state = new NoState();
		}
		else if (activeUnit is EU) {
			//((EU)activeUnit).genRoutes ();
			//state = new NoState();
		}
		Battle.raise (this, new NewTurnAlert((Unit)activeUnit));
	}
	#endregion
	#region Object updates
	public void updateMove() {
		if (((Unit)activeUnit).updateMove()) {
			Battle.raise (this, new MoveObjAlert(new Location2D(activeUnit.oldA, activeUnit.oldB), new Location2D(activeUnit.a, activeUnit.b)));
			state = new NoState();
		}
	}
	public void updateAttack() {
		///
	}
	#endregion
	#region Object actions
	public void undoMove() {
		if (((Unit)activeUnit).moved) {
			((Unit)activeUnit).undoMove();
			state = new NoState();
			Battle.raise (this, new MoveObjAlert(new Location2D(activeUnit.a, activeUnit.b), new Location2D(activeUnit.oldA, activeUnit.oldB)));
		}
	}
	public void startMove(Tile tile) {
		if (((Unit)activeUnit).routes.ContainsKey (new Location2D(tile.a, tile.b))) {
			state = new MovingState();
			((Unit)activeUnit).startMove(tile);
			Battle.raise (this, new StartObjMoveAlert((Unit)activeUnit));
		}
	}
	#endregion

}
