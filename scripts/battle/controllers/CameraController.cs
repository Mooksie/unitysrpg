using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : Raisables {
	
	public float INIT_CAMERA_ZOOM = 20f;
	public Vector2 INIT_CAMERA_ROTATION = new Vector2(0f, 30f);
	
	public const float cameraKeyMoveSpeed = 10f;
	public const float cameraMouseMoveSpeed = 25f;
	
	public const float cameraKeyRotateSpeedH = 180f;
	public const float cameraKeyRotateSpeedV = 80f;
	public const float cameraMouseRotateSpeedH = 12f;
	public const float cameraMouseRotateSpeedV = 6f;
	public const float cameraMinRotationV = 20f;
	public const float cameraMaxRotationV = 60f;
	
	public const float cameraMinZoom = 8f;
	public const float cameraMaxZoom = 25f;
	public const float cameraKeyZoomSpeed = 25f;
	public const float cameraMouseZoomSpeed = 480f;
	
	public Vector3 cameraTargetPos = new Vector3();
	public Vector2 cameraRotation = new Vector2();
	public float cameraZoom = 0f;
	
	public const int mouseHoverWidth = 20;
	public const float mouseHoverLimit = 0.2f;
	public float mouseHoverCount = 0f;
	
	public const float centreTime = 0.2f;
	
	public bool autoMoving = false;
	public Vector3 autoMoveDest;
	public Vector3 autoMoveAmount;
	public Vector3 autoMoveDir;
	public float autoMoveTime;
	public bool autoRotating = false;
	public Vector2 autoRotateDest;
	public Vector2 autoRotateAmount;
	public Vector2 autoRotateProgress;
	public Vector2 autoRotateDir;
	public float autoRotateTime;
	public bool autoZooming = false;
	public float autoZoomDest;
	public float autoZoomAmount;
	public int autoZoomDir;
	public float autoZoomTime;
	
	Transform cameraTransform;
	Transform targetTransform;
	
	void Start() {
		Battle.subscribe ("newTurn", this);
		cameraTransform = GameObject.Find("Camera").transform;
		targetTransform = this.transform;
		
		moveCameraAbs (new Vector3(0f, 0f, 0f));
		rotateCameraAbs (INIT_CAMERA_ROTATION);
		zoomCameraAbs (INIT_CAMERA_ZOOM);
	}
	void Update () {
		if (!(autoMoving || autoRotating || autoZooming)) {
			moveMouse ();
			moveKey ();
		} else {
			updateAutoMove ();
			updateAutoRotate ();
			updateAutoZoom ();
		}
	}
	public override void raise(Raisables source, Alert alert) {
		if (alert is NewTurnAlert) {
			Unit moveChar = ((NewTurnAlert)alert).unit;
			if ((State.settings.newTurnCentreMode == 2 && moveChar is PU) || (State.settings.newTurnCentreMode == 1)) {
				startAutoMoveAbs(new Location(moveChar.a, moveChar.b, moveChar.h), centreTime);
			}
		}
	}
	#region Input processing
	void moveMouse() {
		if (Input.GetMouseButton(1)) {
			Vector3 dir = (targetTransform.position - cameraTransform.position).normalized * cameraMouseMoveSpeed * Time.deltaTime;
			moveCameraRel (new Vector3(-dir.z, 0f, dir.x) * Input.GetAxis("Mouse X"));
			moveCameraRel (new Vector3(-dir.x, 0f, -dir.z) * Input.GetAxis("Mouse Y"));
		} else
		if (Input.GetMouseButton(2)) {
			rotateCameraRel (new Vector2(Input.GetAxis("Mouse X") * cameraMouseRotateSpeedH, -Input.GetAxis("Mouse Y") * cameraMouseRotateSpeedV));
		}
		zoomCameraRel (-Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime * cameraMouseZoomSpeed);
		handleMouseEdgeHover();
	}
	void moveKey() {
		if (Input.GetKey (State.settings.keyBindings["cameraRotateLeft"])) {
			rotateCameraRel (new Vector2(Time.deltaTime * cameraKeyRotateSpeedH, 0f));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraRotateRight"])) {
			rotateCameraRel (new Vector2(-Time.deltaTime * cameraKeyRotateSpeedH, 0f));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraRotateUp"])) {
			rotateCameraRel (new Vector2(0f, Time.deltaTime * cameraKeyRotateSpeedV));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraRotateDown"])) {
			rotateCameraRel (new Vector2(0f, -Time.deltaTime * cameraKeyRotateSpeedV));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraZoomIn"])) {
			zoomCameraRel (-Time.deltaTime * cameraKeyZoomSpeed);
		}
		if (Input.GetKey (State.settings.keyBindings["cameraZoomOut"])) {
			zoomCameraRel (Time.deltaTime * cameraKeyZoomSpeed);
		}
		Vector3 dir = (targetTransform.position - cameraTransform.position).normalized * cameraKeyMoveSpeed * Time.deltaTime;
		if (Input.GetKey (State.settings.keyBindings["cameraMoveUp"])) {
			moveCameraRel (new Vector3(dir.x, 0f, dir.z));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraMoveDown"])) {
			moveCameraRel (new Vector3(-dir.x, 0f, -dir.z));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraMoveLeft"])) {
			moveCameraRel (new Vector3(-dir.z, 0f, dir.x));
		}
		if (Input.GetKey (State.settings.keyBindings["cameraMoveRight"])) {
			moveCameraRel (new Vector3(dir.z, 0f, -dir.x));
		}
	}
	void handleMouseEdgeHover() {
		bool onScreenH = 0 <= Input.mousePosition.x && Input.mousePosition.x <= Screen.width;
		bool onScreenV = 0 <= Input.mousePosition.y && Input.mousePosition.y <= Screen.height;
		bool leftBand = Input.mousePosition.x <= mouseHoverWidth;
		bool rightBand = (Screen.width - mouseHoverWidth) <= Input.mousePosition.x;
		bool topBand = Input.mousePosition.y <= mouseHoverWidth;
		bool bottomBand = (Screen.height - mouseHoverWidth) <= Input.mousePosition.y;
		if (!(onScreenH && onScreenV) || !(leftBand || rightBand || topBand || bottomBand)) {
			mouseHoverCount = 0f;
		} else
		if (mouseHoverCount >= mouseHoverLimit) {
			Vector3 dir = (targetTransform.position - cameraTransform.position).normalized * cameraMouseMoveSpeed * Time.deltaTime;
			if (topBand) {
				moveCameraRel (new Vector3(-dir.x, 0f, -dir.z));
			} else
			if (bottomBand) {
				moveCameraRel (new Vector3(dir.x, 0f, dir.z));
			} 
			if (leftBand) {
				moveCameraRel (new Vector3(-dir.z, 0f, dir.x));
			} else
			if (rightBand) {
				moveCameraRel (new Vector3(dir.z, 0f, -dir.x));
			} 
		} else {
			mouseHoverCount += Time.deltaTime;
		}
	}
	#endregion
	#region AutoMove
	void startAutoMoveAbs(Location newLoc, float moveTime) {
		if (!(autoMoving)) {
			newLoc.a = Mathf.Clamp (newLoc.a, -TileManager.mapSize, TileManager.mapSize);
			newLoc.b = Mathf.Clamp (newLoc.b, -TileManager.mapSize, TileManager.mapSize);
			newLoc.h = Mathf.Clamp (Mathf.Abs(newLoc.h), 0, 99);
			Vector3 newPos = newLoc.toPosition();
			if (newPos != cameraTargetPos) {
				autoMoving = true;
				autoMoveTime = moveTime;
				autoMoveDest = newPos;
				autoMoveAmount = autoMoveDest - targetTransform.position;
				autoMoveDir = new Vector3(autoMoveAmount.x > 0 ? 1 : -1, autoMoveAmount.y > 0 ? 1 : -1, autoMoveAmount.z > 0 ? 1 : -1);
				autoMoveAmount = new Vector3(Mathf.Abs (autoMoveAmount.x), Mathf.Abs (autoMoveAmount.y), Mathf.Abs (autoMoveAmount.z));
			}
		}
	}
	void updateAutoMove() {
		if (autoMoving) {
			Vector3 move = autoMoveAmount * (Time.deltaTime / autoMoveTime);
			if (Vector3.Distance(autoMoveDest, cameraTargetPos) <= move.magnitude) {
			//if ((move.x > Mathf.Abs (autoMoveDest.x - cameraTargetPos.x)) || (move.y > Mathf.Abs (autoMoveDest.y - cameraTargetPos.y)) || (move.z > Mathf.Abs (autoMoveDest.z - cameraTargetPos.z))) {
				autoMoving = false;
				autoMoveTime = 0f;
				moveCameraAbs (autoMoveDest);
			} else {
				moveCameraRel(new Vector3(move.x * autoMoveDir.x, move.y * autoMoveDir.y, move.z * autoMoveDir.z));
			}
		}
	}
	void startAutoRotateAbs(Vector2 newRotation, int rotateDirH, float rotateTime) {
		if (!(autoRotating)) {
			Vector2 newRotationAbs = new Vector2((newRotation.x + 360) % 360, Mathf.Clamp (newRotation.y, cameraMinRotationV, cameraMaxRotationV));
			if (newRotationAbs != cameraRotation) {
				autoRotating = true;
				autoRotateTime = rotateTime;
				autoRotateDest = newRotationAbs;
				autoRotateAmount = newRotation - cameraRotation;
				if (rotateDirH == 0) { // Find the fastest way to the target
					rotateDirH = 1;
					if (-180 < autoRotateAmount.x && autoRotateAmount.x < 180) {rotateDirH = -1;} 
				}
				if ((autoRotateAmount.x * rotateDirH) < 0) {
					autoRotateAmount.x += 360 * rotateDirH;
				}
				autoRotateDir = new Vector2(rotateDirH, (autoRotateAmount.y > 0 ? 1 : -1));
				autoRotateAmount = new Vector2(Mathf.Abs (autoRotateAmount.x), Mathf.Abs (autoRotateAmount.y));
				autoRotateProgress = new Vector2();
			}
		}
	}
	void updateAutoRotate() {
		if (autoRotating) {
			Vector2 rotate = autoRotateAmount * (Time.deltaTime / autoRotateTime);
			if ((rotate.x + autoRotateProgress.x > autoRotateAmount.x) || (rotate.y + autoRotateProgress.y > autoRotateAmount.y)) {
				autoRotating = false;
				autoRotateTime = 0f;
				rotateCameraAbs (autoRotateDest);
			} else {
				rotateCameraRel(new Vector2(rotate.x * autoRotateDir.x, rotate.y * autoRotateDir.y));
				autoRotateProgress += rotate;
			}
		}
	}
	void startAutoZoomAbs(float newZoom, float zoomTime) {
		if (!(autoZooming)) {
			newZoom = Mathf.Clamp (newZoom, cameraMinZoom, cameraMaxZoom);
			if (newZoom != cameraZoom) {
				autoZooming = true;
				autoZoomTime = zoomTime;
				autoZoomDest = newZoom;
				autoZoomAmount = Mathf.Abs(newZoom - cameraZoom);
				autoZoomDir = newZoom > cameraZoom ? 1 : -1;
			}
		}
	}
	void updateAutoZoom() {
		if (autoZooming) {
			float zoom = autoZoomAmount * (Time.deltaTime / autoZoomTime);
			if ((zoom > Mathf.Abs (autoZoomDest - cameraZoom))) {
				autoZooming = false;
				autoZoomTime = 0f;
				zoomCameraAbs (autoZoomDest);
			} else {
				zoomCameraRel(zoom * autoZoomDir);
			}
		}
	}
	#endregion
	#region Camera movement
	void moveCameraRel(Vector3 moveAmount) {
		moveCameraAbs (moveAmount + cameraTargetPos);
	}
	void moveCameraAbs(Vector3 target) {
		cameraTargetPos = target;
		float h = Mathf.Sqrt(target.x * target.x + target.z * target.z);
		if (h > (TileManager.mapSize - 1)) {
			float r = (TileManager.mapSize - 1) / h;
			cameraTargetPos.x *= r;
			cameraTargetPos.z *= r;
		}
		cameraTargetPos.y = Mathf.Clamp (Mathf.Abs(target.y), 0, 99);
		targetTransform.position = cameraTargetPos;
	}
	void rotateCameraRel(Vector2 rotationAmount) {
		rotateCameraAbs(rotationAmount + cameraRotation);
	}
	void rotateCameraAbs(Vector2 rotation) {
		cameraRotation.x = (rotation.x + 360) % 360;
		cameraRotation.y = Mathf.Clamp (rotation.y, cameraMinRotationV, cameraMaxRotationV);
		targetTransform.rotation = Quaternion.Euler(cameraRotation.y, cameraRotation.x, 0f);
	}
	void zoomCameraRel(float zoomAmount) {
		zoomCameraAbs (zoomAmount + cameraZoom);
	}
	void zoomCameraAbs(float zoom) {
		cameraZoom = Mathf.Clamp (zoom, cameraMinZoom, cameraMaxZoom);
		cameraTransform.localPosition = new Vector3(0f, 0f, -cameraZoom);
	}
	#endregion
}