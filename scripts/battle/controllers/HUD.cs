using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HUD : Raisables {
	public Dictionary<string, Texture2D> imageMappings = new Dictionary<string, Texture2D>();
	public List<Texture2D> turnImages = new List<Texture2D>();
	
	void Start() {
		Battle.subscribe("newTurn", this);
		imageMappings.Add("EC1", Resources.Load("textures/turnEC1") as Texture2D);
		imageMappings.Add("MC1", Resources.Load("textures/turnMC1") as Texture2D);
		imageMappings.Add("MC2", Resources.Load("textures/turnMC2") as Texture2D);
		updateTurnImages();
	}
	public override void raise(Raisables source, Alert alert) {
		if (alert is NewTurnAlert) {
			updateTurnImages ();
		}
	}
	void updateTurnImages() {
		turnImages = new List<Texture2D>();
		for (int i = 0; i < BattleController.turnQueue.Count; i++) {
			turnImages.Add(imageMappings[BattleController.turnQueue[i].name]);
		}
	}
    void OnGUI()
    {
		for (int i = 0; i < turnImages.Count && i < 6; i++) {
			GUI.DrawTexture(new Rect(1207, 31 + 22 * i, 40, 20), turnImages[i]);
		}
    }
}
