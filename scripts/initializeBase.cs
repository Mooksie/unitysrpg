﻿using UnityEngine;
using System.Collections;

public class initializeBase : MonoBehaviour {
	public float SPLASH_SCREEN_WAIT = 0f;

	void Start () {
		NewGame ();
		StartCoroutine(waiting());
	}
	IEnumerator waiting()
	{
		yield return new WaitForSeconds(SPLASH_SCREEN_WAIT);
		Application.LoadLevel ("battleScene");
	}
	void NewGame() {
		Character c1 = new Character();
		State.characters.Add ("MC1", c1);
		Character c2 = new Character();
		State.characters.Add ("MC2", c2);
	}
	void Load() {
		Saver.Load();
	}
}
